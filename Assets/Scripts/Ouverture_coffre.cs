﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ouverture_coffre : MonoBehaviour
{
    public GameObject coffre;
    public GameObject baton;
    public GameObject trouver_baton;
    private bool fini = true;

    void Start()
    {
    }
    void OnTriggerEnter2D(Collider2D autre)
    {
        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.E))
        {
            if (fini)
            {
                coffre.SetActive(false);
                baton.SetActive(true);
                var texte_trouve = Instantiate(trouver_baton, new Vector3(Screen.width/2, Screen.height/2, 0.0f), transform.rotation, GameObject.FindGameObjectWithTag("Canvas").transform) ;
                Destroy(texte_trouve, 6);
                fini = false;
            }

        }
    }
}

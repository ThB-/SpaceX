﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Script_PV_Ennemi : MonoBehaviour
{
    public float pv = 3.0f;
    public GameObject vie_pleine;
    public GameObject vie_1_fois;
    public GameObject vie_2_fois;
    public GameObject animation_touche;
    public GameObject ennemi;
    public GameObject texte_tuer_ennemi;
    public GameObject cam;

    private Vague wave;

    public int nb_ennemi_a_tuer;

    private bool fini = true;

    void Start()
    {
        wave = cam.GetComponent<Vague>();
    }
    void Update()
    {
        if (pv == 2.0f)
        {
            vie_pleine.SetActive(false);
            vie_1_fois.SetActive(true);
        }
        if (pv == 1.0f)
        {
            vie_1_fois.SetActive(false);
            vie_2_fois.SetActive(true);
        }
        if (pv == 0.0f)
        { 
            var texte = Instantiate(texte_tuer_ennemi, new Vector3(150.0f,150.0f,0.0f),Quaternion.identity, GameObject.FindGameObjectWithTag("Canvas").transform);
            Destroy(ennemi);
            Destroy(texte,3);
            wave.retirer_un();
        }

    }
    void OnTriggerEnter2D(Collider2D autre)
    {
        if (autre.gameObject.tag == "Attaque" && fini == true)
        {
            fini = false;
      
            StartCoroutine(Perte_PV());

            pv -= 1;
        }
    }

    public IEnumerator Perte_PV()
    {
        var touche = Instantiate(animation_touche, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Destroy(touche);
        yield return new WaitForSeconds(1);
        fini = true;
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramasser_baton : MonoBehaviour
{
    public GameObject baton_gui;
    public GameObject baton;


    void OnTriggerEnter2D(Collider2D autre)
    {
        baton.SetActive(false);
        baton_gui.SetActive(true);
    }
}

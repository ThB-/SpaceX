﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tirer_menu : MonoBehaviour
{
    public GameObject missile;
    public float vitesse = 10.0f;
    private bool fini = true;
    public float temps_passe = 0.0f;
    public GameObject anim_degats;

    void Start()
    {
        
    }

    void Update()
    {
        temps_passe += Time.deltaTime;
        transform.Rotate(0,0,vitesse * Time.deltaTime);
        if (temps_passe > 0.2f)
        {
            var tir = Instantiate(missile, transform.position, transform.rotation);
            Destroy(tir, 4);
            temps_passe = 0.0f;
        }
    }

    void OnTriggerEnter2D(Collider2D autre)
    {
        if(autre.gameObject.tag == "Attaque_Ennemi")
        {
            if (fini)
            {
                fini = false;
                StartCoroutine(recevoir_degats());
            }

        }
    }

    public IEnumerator recevoir_degats()
    {
        var degats = Instantiate(anim_degats,transform.position,Quaternion.identity);
        yield return new WaitForSeconds(1);
        Destroy(degats);
        fini = true;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regarder_menu : MonoBehaviour
{
    public GameObject player;
    public GameObject tir;
    public float temps_passe = 0.0f;
    void Start()
    {
        
    }

    void Update()
    {
        temps_passe += Time.deltaTime;
        transform.LookAt(player.transform);
        transform.Rotate(new Vector3(0, -90, -90), Space.Self);
        if(temps_passe > 0.3f)
        {
            var le_tir = Instantiate(tir, transform.position, transform.rotation);
            Destroy(le_tir,3);
            temps_passe = 0.0f;
        }
    }
}

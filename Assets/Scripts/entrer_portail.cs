﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class entrer_portail : MonoBehaviour
{
    public int nb_vague;
    void OnTriggerEnter2D(Collider2D autre)
    {
        if(nb_vague == 0)
        {
            SceneManager.LoadScene("vague_1");
        }
        if(nb_vague == 1)
        {
            SceneManager.LoadScene("game_over");
        }
    }
}

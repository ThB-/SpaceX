﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PV_script : MonoBehaviour
{
    public float pv = 3.0f;
    public GameObject vie_pleine;
    public GameObject vie_1_fois;
    public GameObject vie_2_fois;
    public GameObject animation_touche;
    public GameObject le_vaisseau;
    public GameObject texte_touche;

    private bool fini = true;

    void Start()
    {
        
    }
    void Update()
    {
        if (pv == 2.0f){
            vie_pleine.SetActive(false);
            vie_1_fois.SetActive(true);
        }
        if (pv == 1.0f)
        {
            vie_1_fois.SetActive(false);
            vie_2_fois.SetActive(true);
        }
        if (pv == 0.0f)
        {
            SceneManager.LoadScene("game_over");
        }
    }
    void OnTriggerEnter2D(Collider2D autre)
    {
        if(autre.gameObject.tag == "Attaque_Ennemi" && fini == true)
        {
            fini = false;
            var touche_texte = Instantiate(texte_touche, transform.position,Quaternion.identity, GameObject.FindGameObjectWithTag("Canvas").transform);
            StartCoroutine(Perte_PV());
            Destroy(touche_texte, 3);
            pv -= 1;
        }
    }

    public IEnumerator Perte_PV()
    {
        var touche = Instantiate(animation_touche, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Destroy(touche);
        yield return new WaitForSeconds(1);
        fini = true;
    }
    


}

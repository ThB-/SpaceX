﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deaplcer_Vaisseau : MonoBehaviour
{
    public float vitesse = 2.0f;
    void Start()
    {
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            transform.rotation = Quaternion.Euler(0,0,270);
            transform.Translate(Vector3.up * vitesse * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
            transform.Translate(Vector3.up * vitesse * Time.deltaTime);
        }


        if (Input.GetKey(KeyCode.Z))
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.Translate(Vector3.up * vitesse * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.rotation = Quaternion.Euler(0, 0, 180);
            transform.Translate(Vector3.up * vitesse * Time.deltaTime);
        }


        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.Z))
        {
            transform.rotation = Quaternion.Euler(0, 0, 300);
        }
        if (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S))
        {
            transform.rotation = Quaternion.Euler(0, 0, 235);
        }
        if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.Q))
        {
            transform.rotation = Quaternion.Euler(0, 0, 135);
        }
        if (Input.GetKey(KeyCode.Q) && Input.GetKey(KeyCode.Z))
        {
            transform.rotation = Quaternion.Euler(0, 0, 45);
        }
    }
}

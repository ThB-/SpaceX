﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class faire_Avancer : MonoBehaviour
{
    public float vitesse = 15.0f;

    void Update()
    {
        transform.Translate(Vector3.up * vitesse * Time.deltaTime);
    }
}

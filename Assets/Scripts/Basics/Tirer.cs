﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tirer : MonoBehaviour
{
    public GameObject missile;
    private float temps_passe = 0.0f;
    public GameObject baton;
    public GameObject attaque_speciale;
    public GameObject laser;

    void Update()
    {
        temps_passe += Time.deltaTime;
        if (Input.GetKey(KeyCode.Space) && temps_passe > 0.1f)
        {
            Instantiate(missile, transform.position, transform.rotation);
            temps_passe = 0.0f;
        }

        if (Input.GetKey(KeyCode.R) && temps_passe > 0.1f && baton.activeSelf)
        {
            Instantiate(attaque_speciale, transform.position, transform.rotation);
            temps_passe = 0.0f;
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            laser.SetActive(true);
        }
        else
        {
            laser.SetActive(false);
        }
    }
}

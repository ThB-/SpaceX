﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ennemi_tirer : MonoBehaviour
{
    public GameObject joueur;
    public GameObject missile_ennemi;
    private float temps_passe;


    void Update()
    {
        temps_passe += Time.deltaTime;
        if (temps_passe > 2.0f)
        {
            Instantiate(missile_ennemi,transform.position, transform.rotation);
            temps_passe = 0.0f;
        }
        transform.position = Vector3.MoveTowards(transform.position, joueur.transform.position, Time.deltaTime);
        transform.LookAt(joueur.transform);
        transform.Rotate(new Vector3(0, -90, -90), Space.Self);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vague : MonoBehaviour
{
    public int nb_ennemi_a_tuer;
    public GameObject portail;
    public GameObject coffre;
    public GameObject texte_fin;
    public int nb_vague = 0;

    private bool fini = true;
    void Start()
    {
        
    }


    void Update()
    {
        if(nb_ennemi_a_tuer == 0 && nb_vague == 0)
        {
            if (fini)
            {
                portail.SetActive(true);
                coffre.SetActive(true);
                texte_fin.SetActive(true);
                fini = false;
            }
      
        }
        if(nb_ennemi_a_tuer == 0 && nb_vague == 1)
        {
            portail.SetActive(true);
        }
    }

    public void retirer_un()
    {
        nb_ennemi_a_tuer -= 1;
    }
}
